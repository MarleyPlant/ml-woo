<?php
function ml_woo_settings_page() {
	//Settings Page For Managing API-Key and Group ID

	if ( !current_user_can( 'manage_options' ) )  {
		//If a user doesn't have permissions to view page display an error message
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	echo '<div class="wrap">'; //Open Page Wrapper
	echo '<h1>MailerLite WooCommerce Intergration</h1>';
	echo '<form method="post" action="options.php"> ';
	settings_fields( 'ml_woo-settings-group' );
	do_settings_sections( 'ml_woo-settings-group' );
	?>
	<table class="form-table">
		<tr valign="top">
			<th scope="row">API KEY</th>
			<td><input type="text" name="api-key" value="<?php echo esc_attr( get_option('api-key') ); ?>" /></td>
		</tr>
		<tr valign="top">
			<th scope="row">Group ID</th>
			<td><input type="number" name="group-id" value="<?php echo esc_attr( get_option('group-id') ); ?>" /></td>
		</tr>
	</table>
	<?php
	submit_button(); //Insert Submit Button
	echo '</form>'; //Close Form
	echo '</div>'; //Close Page Wrapper
}
?>