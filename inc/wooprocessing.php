<?php

function woo_order_processing( $order ){
	$order = new WC_Order( $order ); //Get Order From Order ID
	$order = $order->get_data(); //Get User Information From Order

	$groupsApi = (new \MailerLiteApi\MailerLite(get_option('api-key')))->groups(); //Create a MailerLite API Instance
	$subscriber = [ //Set subscriber info based on order info
	    'email' => $order['billing']['email'],
	    'fields' => [
	        'name' => $order['billing']['first_name'],
	        'last_name' => $order['billing']['last_name'],
	        'company' => $order['billing']['company'],
			'phone' => $order['billing']['phone'],
			'zip' => $order['billing']['postcode']
	    ]
	];

	$response = $groupsApi->addSubscriber((int)get_option('group-id'), $subscriber); // Add Subscriber To MailerLite Group
}

add_action( 'woocommerce_order_status_processing', 'woo_order_processing', 10, 1);