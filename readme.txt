=== Ml Woo ===
Contributors: marleyjplant
Tags: Woocommerce, Mailerlite, Email
Donate link: https://marleyplant.com
Requires at least: 4.7
Tested up to: 5.4
Requires PHP: 7
Stable tag: 1.1.0
License: GNUv3
License URI: /LICENESh

Mailerlite Plus Woocommerce, automatically add new customers to email group.
