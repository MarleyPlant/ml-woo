<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://marleyplant.com/project/ml-woo
 * @since             1.1.0
 * @package           Ml_Woo
 *
 * @wordpress-plugin
 * Plugin Name:       Mailerlite-Woo
 * Plugin URI:        https://gitlab.com/MarleyPlant/ml-woo
 * Description:       Add MailerLite Intergration to your WooCommerce Store
 * Version:           1.0.0
 * Author:            Marley Joseph Plant
 * Author URI:        https://marleyplant.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ml-woo
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//Require Libraries
require 'vendor/autoload.php';
require 'inc/settings/mlWooSettingsPage.php';
require 'inc/wooprocessing/.php';

function ml_woo_menu() {
	add_options_page( 'MailerLite WooCommerce Options', 'ML WooCommerce', 'manage_options', 'ml-woo', 'ml_woo_settings_page' );

	add_action( 'admin_init', 'ml_woo_settings' ); //Register Settings

}

function ml_woo_settings(){
	//Create settings to store User information in the Wordpress database
	register_setting( 'ml_woo-settings-group', 'api-key' );
	register_setting( 'ml_woo-settings-group', 'group-id' );
}

add_action( 'admin_menu', 'ml_woo_menu' );
